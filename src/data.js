// dont change this file
exports.orders = [
  { id: 1, quantity: 10, article: "A" },
  { id: 2, quantity: 5, article: "A" },
  { id: 3, quantity: 3, article: "B" },
  { id: 4, quantity: 15, article: "B" },
  { id: 5, quantity: 2, article: "C" },
];

exports.prices = [
  { article: "A", price: 5 },
  { article: "B", price: 12 },
  { article: "C", price: 9 },
];

exports.invoices = [
  { user: "laurent@neoxam.com", order: 1 },
  { user: "laurent@neoxam.com", order: 3 },
  { user: "laurent@neoxam.com", order: 5 },
  { user: "val@neoxam.com", order: 2 },
  { user: "jood@neoxam.com", order: 4 },
];

exports.expected = [
  { user: "jood@neoxam.com", total: 180 },
  { user: "laurent@neoxam.com", total: 104 },
  { user: "val@neoxam.com", total: 25 },
];
