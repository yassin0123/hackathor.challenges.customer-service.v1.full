const { orders, prices, invoices, expected } = require("../src/data");
const getUsersPurshases = require("../src/app");

describe("test app", () => {
  it("1-1| test with default data", () => {
    const result = getUsersPurshases(orders, prices, invoices);

    expect(result).toEqual(expected);
  });

  it("1-2| test with few users", () => {
    const laurenInvoices = [
      { user: "laurent@neoxam.com", order: 1 },
      { user: "laurent@neoxam.com", order: 3 },
      { user: "laurent@neoxam.com", order: 5 },
    ];

    const laurenExpected = [{ user: "laurent@neoxam.com", total: 104 }];

    const result = getUsersPurshases(orders, prices, laurenInvoices);

    expect(result).toEqual(laurenExpected);
  });
});
